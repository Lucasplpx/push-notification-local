import React, { useEffect } from 'react';
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native';
import {
  handleScheduleNotification,
  showNotification,
  handleCancel,
} from './src/notification';

import BackgroundTimer from 'react-native-background-timer';

const App = () => {
  useEffect(() => {
    BackgroundTimer.runBackgroundTimer(() => {
      console.log('Fazendo requisicao... every 5 seconds ');
      handleScheduleNotification(
        'Nova Mensagem',
        'Atualizacao a 5sec, é somente um teste *-*',
      );
    }, 5000);
  }, []);

  return (
    <View style={sytles.container}>
      <Text>Push Notification</Text>
      <TouchableOpacity
        activeOpacity={0.6}
        onPress={() =>
          showNotification('Atualizacao Orcamento', 'Lucas teste...')
        }>
        <View style={sytles.button}>
          <Text style={sytles.buttonTitle}>Click me to get notification</Text>
        </View>
      </TouchableOpacity>

      <TouchableOpacity
        activeOpacity={0.6}
        onPress={() =>
          handleScheduleNotification(
            'Nova Mensagem',
            'Atualizacao a 5sec, é somente um teste *-*',
          )
        }>
        <View style={sytles.button}>
          <Text style={sytles.buttonTitle}>
            Click me to get notification after 5sec.
          </Text>
        </View>
      </TouchableOpacity>

      <TouchableOpacity activeOpacity={0.6} onPress={() => handleCancel()}>
        <View style={sytles.button}>
          <Text style={sytles.buttonTitle}>
            Click me to cancell all notifications
          </Text>
        </View>
      </TouchableOpacity>
    </View>
  );
};

export default App;

const sytles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  button: {
    padding: 16,
    backgroundColor: '#6292d1',
    borderRadius: 24,
    marginTop: 16,
  },
  buttonTitle: {
    color: '#ffffff',
  },
});
