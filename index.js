/**
 * @format
 */

import { AppRegistry } from 'react-native';
import App from './App';
import { name as appName } from './app.json';
import PushNotificationConfig from './src/config/PushNotificationConfig';

PushNotificationConfig();

AppRegistry.registerComponent(appName, () => App);
